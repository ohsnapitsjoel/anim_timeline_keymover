The timeline Key Mover tool is a compact utility for moving keyframes directly in the Blender timeline.  The Key Copier tool allows the user to copy frames of the selected object or selected bones from the current frame, to move them in the timeline.  
  
It includes a control for the sensitivity of the cursor position relative to mouse movement, and an option to disable the movement of the current frame position while moving the selected keyframe.  The user can choose to move only keys on the current frame, or all the frames in the playback range (which can be temporarily shortened or lengthened using the P/Alt+P hotkeys in the timeline) for all selected objects/bones.
  
To install: Place the anim_timeline_keymover.py file in Blender's 2.6x/scripts/addons directory. Enable in the Animation category of Blender's user preferences.  
  
The tool and controls are found in the header bar of the timeline.

